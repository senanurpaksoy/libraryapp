package com.kly.libapp.service.mapper;

import com.kly.libapp.domain.Author;
import com.kly.libapp.domain.Book;
import com.kly.libapp.domain.Category;
import com.kly.libapp.service.dto.AuthorDTO;
import com.kly.libapp.service.dto.BookDTO;
import com.kly.libapp.service.dto.CategoryDTO;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Book} and its DTO {@link BookDTO}.
 */
@Mapper(componentModel = "spring")
public interface BookMapper extends EntityMapper<BookDTO, Book> {
    @Mapping(target = "author", source = "author", qualifiedByName = "authorId")
    @Mapping(target = "categories", source = "categories", qualifiedByName = "categoryIdSet")
    BookDTO toDto(Book s);

    @Mapping(target = "removeCategory", ignore = true)
    Book toEntity(BookDTO bookDTO);

    @Named("authorId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    AuthorDTO toDtoAuthorId(Author author);

    @Named("categoryId")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    CategoryDTO toDtoCategoryId(Category category);

    @Named("categoryIdSet")
    default Set<CategoryDTO> toDtoCategoryIdSet(Set<Category> category) {
        return category.stream().map(this::toDtoCategoryId).collect(Collectors.toSet());
    }
}
