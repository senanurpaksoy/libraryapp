/**
 * View Models used by Spring MVC REST controllers.
 */
package com.kly.libapp.web.rest.vm;
