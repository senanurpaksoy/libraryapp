import { IAuthor } from 'app/shared/model/author.model';
import { ICategory } from 'app/shared/model/category.model';

export interface IBook {
  id?: number;
  name?: string;
  price?: number;
  author?: IAuthor | null;
  categories?: ICategory[] | null;
}

export const defaultValue: Readonly<IBook> = {};
