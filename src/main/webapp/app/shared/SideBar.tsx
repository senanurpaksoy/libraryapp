import React, {useState} from 'react';
import { Link } from 'react-router-dom';
import { Translate } from 'react-jhipster';
import {FaBars, FaBook, FaClipboard, FaTimes, FaUserTag} from 'react-icons/fa';

import './SideBar.css'; // CSS dosyasını ekleyin
const SideBar = () => {
  // Menü öğelerini bir dizi içinde tanımlayın
  const menuItems = [
    { labelKey: 'global.menu.entities.author', path: '/author', icon: <FaUserTag /> },
    { labelKey: 'global.menu.entities.book', path: '/book', icon: <FaBook /> },
    { labelKey: 'global.menu.entities.category', path: '/category', icon: <FaClipboard /> },
  ];
  // State tanımı
  const [isSidebarOpen, setIsSidebarOpen] = useState(false);

  // Sidebar'ı açma fonksiyonu
  const openSidebar = () => {
    setIsSidebarOpen(true);
  };

  // Sidebar'ı kapatma fonksiyonu
  const closeSidebar = () => {
    setIsSidebarOpen(false);
  };
  const sidebarClassName = isSidebarOpen ? 'open' : 'closed';
  return (
    <div className={`container-fluid ${sidebarClassName}`}>
      {/* Açma/Kapama düğmesi */}
      <button className="hamburger-icon" onClick={isSidebarOpen ? closeSidebar : openSidebar}>
        {isSidebarOpen ? <FaTimes /> : <FaBars />}
      </button>
      <div className="row flex-nowrap">
        <div className="col-md-12 col-lg-2 min-vh-100 d-flex flex-column justify-content-between">
          <div className="bg-dark p-2">
            <ul className="nav nav-pills flex-column mt-4">
              {menuItems.map((item, index) => (
                <li className="nav-item py-2 py-sm-0" key={index}>
                  <Link to={item.path} className="nav-link text-white">
                    {item.icon}
                    <span className="fs-4 ms-3 d-none d-sm-inline">
                      <Translate contentKey={item.labelKey} />
                    </span>
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SideBar;
